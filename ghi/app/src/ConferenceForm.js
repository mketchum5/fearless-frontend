import React from 'react';


class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            locations: [],
            name: "",
            starts: "",
            ends: "",
            max_attendees: "",
            max_presentations: "",
            location: "",
            description: "",
        };
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartsChange = this.handleStartsChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value})
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value})
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value})
    }

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({max_attendees: value})
    }

    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({max_presentations: value})
    }

    handleStartsChange(event) {
        const value = event.target.value;
        this.setState({starts: value})
    }

    handleEndsChange(event) {
        const value = event.target.value;
        this.setState({ends: value})
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        delete data.locations;

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok){
                const newConference = await response.json();
                console.log(newConference);

                this.setState({
                name: "",
                starts: "",
                ends: "",
                max_attendees: "",
                max_presentations: "",
                location: "",
                description: "",})
            }

    }

    async componentDidMount() {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();

        this.setState({locations: data.locations})
    }

}
    render() {
        return (
            <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form id="create-conference-form">
              <div className="form-floating mb-3">
                <input placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input placeholder="Start date" name="starts" input type="date" id="starts" className="form-control" />
                <label htmlFor="starts">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="End date" name="ends" input type="date" id="ends" className="form-control" />
                <label htmlFor="ends">End date</label>
              </div>
              <div className="mb-3">
                <label htmlFor="exampleFormControlTextarea1" className="form-label"></label>
                <textarea className="form-control" placeholder="Description" id="exampleFormControlTextarea1" rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Maximum presentations" name="max_presentations" required type="number" id="max_presentations" className="form-control" />
                <label htmlFor="room_count">Maximum presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxAttendeesChange} placeholder="Maximum attendees" name="max_attendees" required type="number" id="max_attendees" className="form-control" />
                <label htmlFor="room_count">Maximum attendees</label>
              </div>
              <div className="mb-3">
                <select required id="location" name="location" className="form-select">
                  <option value="">Choose a location</option>
                  {this.state.locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
        );
    }
}

export default ConferenceForm
